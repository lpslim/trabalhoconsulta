-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema catalogores
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema catalogores
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `catalogores` DEFAULT CHARACTER SET utf8 ;
USE `catalogores` ;

-- -----------------------------------------------------
-- Table `catalogores`.`categoriaProduto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalogores`.`categoriaProduto` (
  `id` INT(11) NOT NULL,
  `nome` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catalogores`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalogores`.`cliente` (
  `id` INT(11) NOT NULL,
  `nome` VARCHAR(60) NOT NULL,
  `cpf` VARCHAR(11) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `login` VARCHAR(20) NOT NULL,
  `senha` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `cliente_cpf_UNIQUE` (`cpf` ASC),
  UNIQUE INDEX `cliente_login_UNIQUE` (`login` ASC),
  UNIQUE INDEX `cliente_senha_UNIQUE` (`senha` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catalogores`.`categoriaEstabelecimento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalogores`.`categoriaEstabelecimento` (
  `id` INT NOT NULL,
  `descricao` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `catalogores`.`estabelecimento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalogores`.`estabelecimento` (
  `id` INT(11) NOT NULL,
  `razaoSocial` VARCHAR(60) NOT NULL,
  `nomeFantasia` VARCHAR(40) NOT NULL,
  `quantidadeMesa` VARCHAR(3) NULL,
  `cnpj` VARCHAR(11) NOT NULL,
  `horarioAbertura` TIME NOT NULL,
  `apresentacao` VARCHAR(45) NULL,
  `horarioFechamento` TIME NOT NULL,
  `diasFuncionamento` VARCHAR(30) NOT NULL,
  `cliente` INT(11) NOT NULL,
  `categoriaEstabelecimento` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `estabelecimento_cnpj_UNIQUE` (`cnpj` ASC),
  INDEX `fk_Estabelecimento_Cliente1_idx` (`cliente` ASC),
  INDEX `fk_estabelecimento_categoriaEstabelecimento1_idx` (`categoriaEstabelecimento` ASC),
  CONSTRAINT `fk_Estabelecimento_Cliente1`
    FOREIGN KEY (`cliente`)
    REFERENCES `catalogores`.`cliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_estabelecimento_categoriaEstabelecimento1`
    FOREIGN KEY (`categoriaEstabelecimento`)
    REFERENCES `catalogores`.`categoriaEstabelecimento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catalogores`.`endereco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalogores`.`endereco` (
  `id` INT(11) NOT NULL,
  `latitude` VARCHAR(45) NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  `cep` VARCHAR(10) NOT NULL,
  `rua` VARCHAR(100) NOT NULL,
  `numero` VARCHAR(5) NOT NULL,
  `bairro` VARCHAR(20) NOT NULL,
  `estabelecimento` INT(11) NOT NULL,
  `complemento` VARCHAR(40) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Endereco_Estabelecimento1_idx` (`estabelecimento` ASC),
  CONSTRAINT `fk_Endereco_Estabelecimento1`
    FOREIGN KEY (`estabelecimento`)
    REFERENCES `catalogores`.`estabelecimento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catalogores`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalogores`.`produto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(40) NOT NULL,
  `descricao` VARCHAR(70) NULL,
  `valor` VARCHAR(15) NOT NULL,
  `tempoPreparo` VARCHAR(10) NULL,
  `categoriaProduto` INT(11) NOT NULL,
  `ingrediente` VARCHAR(200) NULL,
  `estabelecimento` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Produto_Categoria1_idx` (`categoriaProduto` ASC),
  INDEX `fk_produto_estabelecimento1_idx` (`estabelecimento` ASC),
  CONSTRAINT `fk_Produto_Categoria1`
    FOREIGN KEY (`categoriaProduto`)
    REFERENCES `catalogores`.`categoriaProduto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_produto_estabelecimento1`
    FOREIGN KEY (`estabelecimento`)
    REFERENCES `catalogores`.`estabelecimento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catalogores`.`telefone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalogores`.`telefone` (
  `numero` VARCHAR(20) NOT NULL,
  `cliente` INT(11) NOT NULL,
  PRIMARY KEY (`cliente`, `numero`),
  INDEX `fk_Telefone_Cliente1_idx` (`cliente` ASC),
  CONSTRAINT `fk_Telefone_Cliente1`
    FOREIGN KEY (`cliente`)
    REFERENCES `catalogores`.`cliente` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
